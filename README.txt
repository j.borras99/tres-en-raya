Programa que genera i mostra l'espai d'estats pel joc de "tres en ralla".
Un cop introduït per teclat l'estat del joc, en un moment donat, el programa
genera l'espai d'estats usant un arbre ordinari. Finalment fa un recorregut en
amplitud de l'arbre i mostre l'espai d'estats.