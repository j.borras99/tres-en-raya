
package body darbolordinario is
   -- Creara una abre buid
   procedure avacio(t: out arbol) is
      raiz: pnode renames t.raiz;
   begin
      raiz := null;
   end avacio;

    -- Retornara el valor guardat dins l'arrel
   function raiz(t: in arbol) return elem is
   begin
      return t.raiz.x;
   exception
         when Constraint_Error => raise mal_uso;
   end raiz;

   -- Ens dira si l'arbre es troba buid o no.
   function esta_vacio(t: in arbol) return Boolean is
      raiz: pnode renames t.raiz;
   begin
      return raiz = null;
   end esta_vacio;

   -- Afegira un fill al pare. En cas de que ja tengui un fill guardem el pare
   -- en una auxiliar per despr�s poder enlla�ar el fill a ell.
   procedure anadir_hijo(t: in out arbol; x: in elem) is
      raiz: pnode renames t.raiz;
      r, aux: pnode;
   begin
      r := new node;
      r.x := x;
      r.padre := raiz;
      if not e_primer_hijo(t) then
         raiz.primer_hijo := r;
      else
         aux := raiz.primer_hijo;
         while aux.hermano /= null loop
            aux := aux.hermano;
         end loop;
         aux.hermano := r;
      end if;
   exception
         when Storage_Error => raise espacio_desbordado;
   end anadir_hijo;

   -- Mirara si existeix el primer fill del pare
   function e_primer_hijo(t: in arbol) return Boolean is
      raiz: pnode renames t.raiz;
   begin
      return raiz.primer_hijo /= null;
   end e_primer_hijo;

   -- Crea un arbre amb l'element
   procedure atom(t: out arbol; x: in elem) is
      raiz: pnode renames t.raiz;
   begin
      raiz := new node;
      raiz.x := x;
      raiz.padre := null;
      raiz.hermano := null;
      raiz.primer_hijo := null;
   exception
         when Storage_Error => raise espacio_desbordado;
   end atom;

   -- Mira si existeixen germans de l'arbre
   function e_hermano(t: in arbol) return Boolean is
      raiz: pnode renames t.raiz;
   begin
      return raiz.hermano /= null;
   end e_hermano;

   -- Retornara el primer germa del node.
   procedure hermano(t: in arbol; st: out arbol) is
      raiz: pnode renames t.raiz;
   begin
      st.raiz := raiz.hermano;
   exception
         when Constraint_Error => raise mal_uso;
   end hermano;

   -- Mira si existeix un pare de l'arbre
   function e_padre(t: in arbol) return Boolean is
      raiz: pnode renames t.raiz;
   begin
      return raiz.padre /= null;
   end e_padre;

   -- Retornara el pare del node
   procedure padre(t: in arbol; pt: out arbol) is
      raiz: pnode renames t.raiz;
   begin
      pt.raiz := raiz.padre;
   exception
         when Constraint_Error => raise mal_uso;
   end padre;

   -- Retorna el subarbre que penja del primer fill de l'arrel.
   procedure primer_hijo(t: in arbol; ct: out arbol) is
   begin
      ct.raiz := t.raiz.primer_hijo;
   exception
         when Constraint_Error => raise mal_uso;
   end primer_hijo;

   -- Realitza un recorregut d'amplitud de l'arbre.
   procedure amplitud (t: in arbol) is
      package coaPnode is new dcola(pnode);
      use coaPnode;
      cola: coaPnode.cola;
      raiz: pnode renames t.raiz;
      p,aux: pnode;
   begin
      cvacia(cola);
      poner(cola, raiz);
      while not esta_vacia(cola) loop
         p := primero(cola);
         print(p.x);
         if p.primer_hijo/= null then
            poner(cola, p.primer_hijo);
            aux := p.primer_hijo;
            while aux.hermano /= null loop
               poner(cola, aux.hermano);
               aux := aux.hermano;
            end loop;
         end if;
         eliminar_primer(cola);
      end loop;
   exception
      when coaPnode.mal_uso => raise darbolordinario.mal_uso;
   end amplitud;

   -- Realitza un recorregut d'amplitud de l'arbre i el retorna dins una coa.
   procedure amplitud_cola(t: in arbol; q: out cola) is
      package coaPnodo is new dcola(pnode);
      use coaPnodo;
      cola: coaPnodo.cola;
      raiz: pnode renames t.raiz;
      p,aux: pnode;
   begin
      cvacia(cola);
      poner(cola, raiz);
      while not esta_vacia(cola) loop
         p := primero(cola);
         poner(q, p.x);
         if p.primer_hijo/= null then
            poner(cola, p.primer_hijo);
            aux := p.primer_hijo.hermano;
            while aux /= null loop
               poner(cola,aux);
               aux := aux.hermano;
            end loop;
         end if;
         eliminar_primer(cola);
      end loop;
   exception
      when coaPnodo.mal_uso => raise darbolordinario.mal_uso;
   end amplitud_cola;

end darbolordinario;
