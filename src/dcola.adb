package body dcola is

   --Procediment per a buidar la coa.
   procedure cvacia(c: out cola) is
   begin
      c.q := null;
      c.p := null;
   end cvacia;

   --Procediment per a incerir un element dins la coa.
   procedure poner(c: in out cola; x: in elem) is
      r: pnodo;
   begin
      r := new nodo'(x, null);
      if c.p = null then
         c.p := r;
         c.q := r;
      else
         c.p.sig := r;
         c.p := r;
      end if;
   exception
         when Storage_Error => raise espacio_desbordado;
   end poner;

   -- Procediment per a eliminar el primer element de la coa.
   procedure eliminar_primer(c: in out cola) is
   begin
      c.q := c.q.sig;
      if c.q = null then
         c.p := null;
      end if;
   exception
      when Constraint_Error => raise mal_uso;
   end eliminar_primer;

   -- Funci� per agafar el primer element de la coa.
   function primero(c: in cola) return elem is
   begin
      return c.q.x;
   exception
         when Constraint_Error => raise mal_uso;
   end primero;

   --Funci� per a veure si la coa esta buida o no.
   function esta_vacia(c: in cola) return boolean is
   begin
      return c.q = null;
   end esta_vacia;

end dcola;
