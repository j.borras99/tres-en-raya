--Bartomeu Ramis Tarrag�
--Josep Borr�s S�nchez

with dtauler;
use dtauler;

with darbolordinario;

with dcola;

with Ada.Text_IO;
use Ada.Text_IO;
with Ada.Integer_Text_IO;
use Ada.Integer_Text_IO;

procedure Main is
      -- TIPUS estat
  type estat is record
     t: tauler;
     jugador: integer;
  end record;
   -- METODE per a imprimir el tauler
   procedure print (s: in estat) is
   begin
      Put_Line("__________________");
      Put_Line("Jugador: " & s.jugador'Image);
      print(s.t);
      if isJocGuanyat(s.t,s.jugador) then
         Put_Line("JOC GUANYAT ("&s.jugador'Image&" )");
      end if;
   end print;
   -- INSTANCIA coa d'estats.
   package dcolaEstat is new dcola(estat);
   -- INSTANCIA arbre d'estats.
   package darboltauler is new darbolordinario(estat, print, dcolaEstat);
   use darboltauler;
   -- INSTANCIA coa de punters a arbres
   type parbol is access arbol;
   package dcolaarbol is new dcola(parbol);
   use dcolaarbol;

   -- TIPUS cadena
   type cadena is record
      s: String(1..9);
      long: Integer;
   end record;

   -- METODE que crear� un estat a partir d'un String.
   procedure crearEstat(c: in cadena; pl: in Integer; st: in out estat)is
      aux: Integer; -- Jugador que hem de moure.
      celda: tcella;
   begin
      st.jugador := pl;
      celda.fila := 1;
      celda.columna := 1;
      for i in 1..c.long loop
         aux := getJugador(""&c.s(i));
         mouJugador(st.t, aux, celda);
         celda.fila := i/3 + 1;
         celda.columna := (i mod 3) +1;
      end loop;
   end crearEstat;

   -- VARIABLES
   st, st2: estat;
   c: cadena; -- Cadena on es guardar� un String.
   pl: Integer; -- Enter on es guardar� l'enter del jugador.

   ptree, aux, aux2: parbol;
   qtree: cola;
   cel: tcella;
begin
   -- Crea un estat amb un tauler buit
   empty(st2.t);
   empty(st.t);
   ptree := new arbol;
   avacio(ptree.all);
   cvacia(qtree);
   -- Menu del problema
   Put_Line("***************************************");
   Put_Line("            Tres en Raya");
   Put_Line("***************************************");
   New_Line;
   Put_Line("Introdueix un estat inicial: ");
   Get_Line(c.s, c.long);  -- Agafem el String de la disposici� de les fitxes
   Put_Line("Jugador que ha realitzat el darrer moviment: ");
   Get(pl);    -- Agafem l'enter referent al darrer jugador que ha mogut fitxa.
   New_Line;
   New_Line;
   -- Crearem l'estat inicial amb les dades recollides.
   crearEstat(c,pl,st);

   -- INSERIM el primer estat com arrel de l'arbre i primer element de l'arbre.
   atom(ptree.all, st);
   poner(qtree, ptree);




   while not esta_vacia(qtree) loop
      aux:= new arbol;
      aux := primero(qtree);
      eliminar_primer(qtree);
      st:= raiz(aux.all);
      if not isJocGuanyat(st.t, st.jugador) and not isTaulerComplet(st.t)then
         cel.fila:=1;
         cel.columna:=1;
         for i in 1..9 loop
            if isCasellaBuida(st.t, cel) then
               clone(st.t, st2.t);
               case st.jugador is
                  when 1 => st2.jugador:=2;
                  when 2 => st2.jugador:=1;
                  when others => null;
               end case;
               mouJugador(st2.t, st2.jugador, cel);
               anadir_hijo(aux.all, st2);
            end if;
            cel.fila := i/3 + 1;
            cel.columna := (i mod 3) +1;
         end loop;
         if e_primer_hijo(aux.all) then
            aux2 := new arbol;
            primer_hijo(aux.all, aux2.all);
            poner(qtree, aux2);
            while e_hermano(aux2.all) loop
               aux := new arbol;
               hermano(aux2.all, aux.all);
               poner(qtree, aux);
               aux2 := aux;
            end loop;
         end if;

      end if;
   end loop;

   amplitud(ptree.all);

end Main;
