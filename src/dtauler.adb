with Ada.Text_IO;
use Ada.Text_IO;

package body dtauler is

   procedure empty (t: out tauler) is
   begin
      t := (others => (others => peces'First));
   end empty;

   procedure print (t: in tauler) is
      p: peces;
      s: String(1..3);
   begin
      New_Line;
      for i in t'Range(1) loop
         for j in t'Range(2) loop
            p := t(i, j);
            s := p'Image;
            put(s(2) & " ");
         end loop;
         New_line;
      end loop;
   end print;

   procedure mouJugador(t: in out tauler; numJugador: in integer; cella: in tcella) is
   begin
      t(cella.fila, cella.columna) := peces'Val(numJugador);
   end mouJugador;

   function getDimensio(t: in tauler) return integer is
   begin
      return dimensio;
   end getDimensio;

   function getNumJugadors(t: in tauler) return integer is
   begin
      return numJugadors;
   end getNumJugadors;

   function isCasellaBuida(t: in tauler; cella: in tcella) return Boolean is
   begin
      return t(cella.fila, cella.columna) = peces'First;
   end isCasellaBuida;

   procedure clone (t1: in tauler; t2: out tauler) is
   begin
      t2 := t1;
   end clone;

   function getJugador(p: in string) return integer is
   begin
      return peces'Pos(peces'Value("'"& p &"'"));
   end getJugador;

   function isTaulerComplet(t: in tauler) return Boolean is
      celda: tcella;
   begin
      celda.fila:=1;
      celda.columna := 1;
      for i in 1..9 loop
         if isCasellaBuida(t, celda) then
            return False;
         end if;
         celda.fila := i/3 + 1;
         celda.columna := (i mod 3) +1;
      end loop;
      return True;
   end isTaulerComplet;

   -- Funci� que retorna si el tauler cont�
   -- una disposici� de peces pel jugador 'jugador'
   -- que formin una l�nia (horitzontal o vertical)
   function isLinia (t: in tauler; jugador: in integer) return Boolean is
      pl: peces;
      cont: Integer := 0;
   begin
      pl:=peces'Val(jugador);
      for i in 1..dimensio loop
         for j in 1..dimensio loop
            if pl = t(i,j) then
               cont := cont + 1;
            end if;
         end loop;
         if cont = 3 then
            return True;
         end if;
         cont := 0;
      end loop;
      for i in 1..dimensio loop
         for j in 1..dimensio loop
            if pl = t(j,i) then
               cont := cont + 1;
            end if;
         end loop;
         if cont = 3 then
            return True;
         end if;
         cont := 0;
      end loop;
      return False;
   end isLinia;

   -- Funci� que retorna si el tauler cont�
   -- una disposici� de peces pel jugador 'jugado'
   -- que formin una diagonal (normal o inversa)
   function isDiagonal (t: in tauler; jugador: in integer) return Boolean is
      pl: peces;
      cont: Integer := 0;
      j: Integer :=1;
   begin
      pl:=peces'Val(jugador);
      for i in 1..dimensio loop
            if pl = t(i,i) then
               cont := cont + 1;
            end if;
         end loop;
         if cont = 3 then
            return True;
         end if;
      cont := 0;
      for i in reverse 1..dimensio loop
         if pl = t(j,i) then
               cont := cont + 1;
         end if;
         j := j + 1;
      end loop;
      if cont = 3 then
            return True;
      end if;
      return False;
   end isDiagonal;

   function isJocGuanyat (t: in tauler; jugador: in integer) return boolean is
   begin
      return isLinia(t,jugador) or isDiagonal(t,jugador);
   end isJocGuanyat;

   function trobarCasellaBuida (t : in tauler; idx: in integer) return tcella is
      i, ix: Integer;
      c: tcella;
   begin
      i:= 1;
      ix := idx;
      c.fila:=1;
      c.columna := 1;
      while (i < 9) and (ix /= 0) loop

         if isCasellaBuida(t, c) then
            ix := ix-1;
         end if;
         if ix /= 0 then
            c.fila := i/3 + 1;
            c.columna := (i mod 3) +1;
         end if;
         i:=i+1;

      end loop;
      return c;
   end trobarCasellaBuida;

end dtauler;
