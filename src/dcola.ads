generic
   type elem is private;

package dcola is

   type cola is limited private;
   mal_uso: exception;
   espacio_desbordado: exception;

   procedure cvacia(c: out cola);
   procedure poner(c: in out cola; x: in elem);
   procedure eliminar_primer(c: in out cola);
   function esta_vacia(c: in cola) return boolean;
   function primero(c: in cola) return elem;

private
   type nodo;
   type pnodo is access nodo;
   type nodo is record
      x: elem;
      sig: pnodo;
   end record;
   type cola is record
      q,p: pnodo;
   end record;
end dcola;
